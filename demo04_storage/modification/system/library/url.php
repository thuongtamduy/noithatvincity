<?php
/**
 * @package		OpenCart
 * @author		Daniel Kerr
 * @copyright	Copyright (c) 2005 - 2017, OpenCart, Ltd. (https://www.opencart.com/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.opencart.com
*/

/**
* URL class
*/
class Url {

          // Set this TRUE if you want to force keyword updates on every change
          // When TRUE, you won't be able to customize keywords
        public function slugify($str){
           $a = array('Ọ','Õ','Ỏ','Ò','Ó','ọ','õ','ỏ','ò','ó','Ỵ','Ỹ','Ỷ','Ỳ','Ý','ỵ','ỹ','ỷ','ỳ','ý','Ự','Ữ','Ử','Ừ','Ứ','Ụ','Ũ','Ủ','Ù','Ú','ự','ữ','ử','ừ','ứ','ụ','ũ','ủ','ù','ú','Ị','Ĩ','Ỉ','Ì','Í','ị','ĩ','ỉ','ì','í','Ệ','Ễ','Ể','Ề','Ế','Ê','ệ','ễ','ể','ề','ế','ê','Ẹ','Ẽ','Ẻ','È','É','ẹ','ẽ','ẻ','è','é','Ậ','Ẫ','Ẩ','Ầ','Ấ','Â','ậ','ẫ','ẩ','ầ','ấ','â','Ặ','Ẵ','Ẳ','Ằ','Ắ','Ă','ặ','ẵ','ẳ','ằ','ắ','ă','Ạ','Ả','Ả','À','Á','A','ạ','ã','ả','à','á','a','Ộ','Ỗ','Ổ','Ồ','Ố','Ô','ộ','ỗ','ổ','ồ','ố','ô','Ợ','Ỡ','Ờ','Ớ','Ở','ợ','ỡ','ờ','ớ','ở','À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ','Ā','ā','Ă','ă','Ą','ą','Ć','ć','Ĉ','ĉ','Ċ','ċ','Č','č','Ď','ď','Đ','đ','Ē','ē','Ĕ','ĕ','Ė','ė','Ę','ę','Ě','ě','Ĝ','ĝ','Ğ','ğ','Ġ','ġ','Ģ','ģ','Ĥ','ĥ','Ħ','ħ','Ĩ','ĩ','Ī','ī','Ĭ','ĭ','Į','į','İ','ı','Ĳ','ĳ','Ĵ','ĵ','Ķ','ķ','Ĺ','ĺ','Ļ','ļ','Ľ','ľ','Ŀ','ŀ','Ł','ł','Ń','ń','Ņ','ņ','Ň','ň','ŉ','Ō','ō','Ŏ','ŏ','Ő','ő','Œ','œ','Ŕ','ŕ','Ŗ','ŗ','Ř','ř','Ś','ś','Ŝ','ŝ','Ş','ş','Š','š','Ţ','ţ','Ť','ť','Ŧ','ŧ','Ũ','ũ','Ū','ū','Ŭ','ŭ','Ů','ů','Ű','ű','Ų','ų','Ŵ','ŵ','Ŷ','ŷ','Ÿ','Ź','ź','Ż','ż','Ž','ž','ſ','ƒ','Ơ','ơ','Ư','ư','Ǎ','ǎ','Ǐ','ǐ','Ǒ','ǒ','Ǔ','ǔ','Ǖ','ǖ','Ǘ','ǘ','Ǚ','ǚ','Ǜ','ǜ','Ǻ','ǻ','Ǽ','ǽ','Ǿ','ǿ');
$b = array('O','O','O','O','O','o','o','o','o','o','Y','Y','Y','Y','Y','y','y','y','y','y','U','U','U','U','U','U','U','U','U','U','u','u','u','u','u','u','u','u','u','u','I','I','I','I','I','i','i','i','i','i','E','E','E','E','E','E','e','e','e','e','e','e','E','E','E','E','E','e','e','e','e','e','A','A','A','A','A','A','a','a','a','a','a','a','A','A','A','A','A','A','a','a','a','a','a','a','A','A','A','A','A','A','a','a','a','a','a','a','O','O','O','O','O','O','O','O','O','O','O','o','o','o','o','o','o','o','o','o','o','o','A','A','A','A','A','A','AE','C','E','E','E','E','I','I','I','I','D','N','O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','ae','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','o','u','u','u','u','y','y','A','a','A','a','A','a','C','c','C','c','C','c','C','c','D','d','D','d','E','e','E','e','E','e','E','e','E','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i','I','i','I','i','IJ','ij','J','j','K','k','L','l','L','l','L','l','L','l','l','l','N','n','N','n','N','n','n','O','o','O','o','O','o','OE','oe','R','r','R','r','R','r','S','s','S','s','S','s','S','s','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u','U','u','W','w','Y','y','Y','Z','z','Z','z','Z','z','s','f','O','o','U','u','A','a','I','i','O','o','U','u','U','u','U','u','U','u','U','u','A','a','AE','ae','O','o');
return str_replace($a, $b, $str);
        }
        public function seoURL($args) {
            // Setting url string
            $str = $args['string'];
            $str = html_entity_decode($str, ENT_QUOTES, 'UTF-8');
            $str = $this->slugify($str);
            $str = preg_replace('/([^a-zA-Z0-9]|-)+/', '-', $str);
            $str = trim($str, '-');
            $str = strtolower($str);
            // Avoid duplication
            $db = $args['db'];
            if ($db) {
              $okay = false;
              $counter = 1;
              $modifier = '';
              do {
                if($counter > 1)
                  $modifier = '-' . $counter;
                $result = $db->query("SELECT COUNT(*) as `total` FROM " . DB_PREFIX . "seo_url WHERE keyword = '" . $db->escape($str . $modifier) . "' AND query != '" . $args['query'] . "'");
                if($result->row['total'] == 0) {
                  $str .= $modifier;
                  $okay = true;
                } else
                  $counter++;
              } while($okay == false);
            }
            return $str;
          }
        
	private $url;
	private $ssl;
	private $rewrite = array();
	
	/**
	 * Constructor
	 *
	 * @param	string	$url
	 * @param	string	$ssl
	 *
 	*/
	public function __construct($url, $ssl = '') {
		$this->url = $url;
		$this->ssl = $ssl;
	}

	/**
	 *
	 *
	 * @param	object	$rewrite
 	*/	
	public function addRewrite($rewrite) {
		$this->rewrite[] = $rewrite;
	}

	/**
	 * 
	 *
	 * @param	string		$route
	 * @param	mixed		$args
	 * @param	bool		$secure
	 *
	 * @return	string
 	*/
	public function link($route, $args = '', $secure = false) {
		if ($this->ssl && $secure) {
			$url = $this->ssl . 'index.php?route=' . $route;
		} else {
			$url = $this->url . 'index.php?route=' . $route;
		}
		
		if ($args) {
			if (is_array($args)) {
				$url .= '&amp;' . http_build_query($args);
			} else {
				$url .= str_replace('&', '&amp;', '&' . ltrim($args, '&'));
			}
		}
		
		foreach ($this->rewrite as $rewrite) {
			$url = $rewrite->rewrite($url);
		}
		
		return $url; 
	}
}