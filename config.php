<?php
define('URL_HOME','http://'.$_SERVER['HTTP_HOST'].'/');
define('DIR_HOME', str_replace('\\', '/', dirname(__FILE__)) . "/");

// HTTP
define('HTTP_SERVER',URL_HOME);

// HTTPS
define('HTTPS_SERVER',URL_HOME);

// DIR
define('DIR_APPLICATION', DIR_HOME.'catalog/');
define('DIR_SYSTEM', DIR_HOME.'system/');
define('DIR_IMAGE', DIR_HOME.'image/');
define('DIR_STORAGE', DIR_HOME.'demo04_storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'Kpis@123');
define('DB_DATABASE', 'kpis_noithatvincity');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');