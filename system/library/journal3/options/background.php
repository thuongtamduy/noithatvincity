<?php

namespace Journal3\Options;

use Journal3\Utils\Arr;
use Journal3\Utils\Img;
use Journal3\Utils\Str;

class Background extends Option {

	protected static function parseValue($value, $data = null) {
		$result = array();

		$has_bg = false;

		if (Arr::get($value, 'none') === 'true') {
			return array(
				'background' => 'none',
			);
		}

		if (($v = Color::parseValue(Arr::get($value, 'background-color'))) !== null) {
			$result['background'] = $v;
		}

		if (($v = trim(Arr::get($value, 'gradient')))) {
			if (Str::startsWith($v, '__VAR__')) {
				$v = Arr::get(static::$variables, 'gradient.' . $v);
			}

			$result['gradient'] = $v;
			$has_bg = true;
		}

		if (($v = Image::parseValue(Arr::get($value, 'background-image'))) !== null) {
			$result['background-image'] = "url('" . Img::resize($v) . "')";
			$has_bg = true;

			if (($v = Option::parseValue(Arr::get($value, 'background-position'))) !== null) {
				$result['background-position'] = $v;
			}

			if (($v = Option::parseValue(Arr::get($value, 'background-attachment'))) !== null) {
				$result['background-attachment'] = $v;
			}

			if (($v = Option::parseValue(Arr::get($value, 'background-repeat'))) !== null) {
				$result['background-repeat'] = $v;
			}

			if (($v = Option::parseValue(Arr::get($value, 'background-origin'))) !== null) {
				$result['background-origin'] = $v;
			}

			if (($v = Option::parseValue(Arr::get($value, 'background-size'))) !== null) {
				$result['background-size'] = $v;
			}
		}

		if (!$has_bg) {
//			$result['background-image'] = 'none';
		}

		return $result ? $result : null;
	}

}
