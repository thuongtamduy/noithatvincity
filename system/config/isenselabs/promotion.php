<?php

$_['promotion_moduleName'] 			= 'promotion';
$_['promotion_moduleVersion'] 		= '3.1';
$_['promotion_modulePath'] 			= 'extension/module/promotion';

$_['promotion_modulePath_stats'] 	= 'extension/module/promotionModule/promotion_stats';

$_['promotion_callModel'] 			= 'model_extension_module_promotion';

$_['promotion_extensionLink'] 		= 'marketplace/extension';
$_['promotion_extensionLink_type'] 	= '&type=module';

$_['promotion_token']				= 'user_token';

$_['promotion_setting_model_path']	= 'setting/module';
$_['promotion_setting_model']		= 'model_setting_module';


$_['promotion_totalName']  			= 'total_promotion_total';

$_['promotion_totalPath']  			= 'extension/total/promotion_total';
$_['promotion_totalModel']  		= 'model_extension_total_promotion_total';

$_['promotion_totalsLink']  		= 'marketplace/extension';
$_['promotion_totalsLink_type']  	= '&type=total';

$_['promotion_seo_slug'] = 'promotions';
