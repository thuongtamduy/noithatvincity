<?php 
class ModelExtensionModulePromotion extends Model {

	private $event_group = 'promotion';
	private $module_path = 'extension/module/promotion';

	public function getSetting($code, $store_id = 0) {
	    $this->load->model('setting/setting');
		return $this->model_setting_setting->getSetting($code,$store_id);
	}
  
  	public function editSetting($code, $data, $store_id = 0) {
	    $this->load->model('setting/setting');
		$this->model_setting_setting->editSetting($code,$data,$store_id);
	}
	
  	public function install() {
  		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "promotion` (
						  `id` int(11) NOT NULL AUTO_INCREMENT,
						  `promotion_id` int(11) NOT NULL,
						  `customer_id` int(11) NOT NULL,
						  `order_id` int(11) NOT NULL,
						  `time` varchar(50) NOT NULL,
						  `coupon_code` varchar(50) NOT NULL,
						  PRIMARY KEY (`id`)
						)");

		//$this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=1 WHERE `name` LIKE'%PromotionModule by iSenseLabs%'");
		//$modifications = $this->load->controller('extension/modification/refresh');
  	} 
  
  	public function uninstall() {
		//$this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=0 WHERE `name` LIKE'%PromotionModule by iSenseLabs%'");
		//$modifications = $this->load->controller('extension/modification/refresh');
  	}

  	public function getCategoryNameByID($cat_id){
  		$query = $this->db->query("SELECT name as category_name FROM  `". DB_PREFIX ."category_description` WHERE category_id=".$cat_id)->row;

		return $query['category_name'];
  	}

  	public function getOptionNameByID($option_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE o.option_id = '" . (int)$option_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getOptionValuesNameByID($option_id) {
		$option_value_data = array();

		$option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE ov.option_id = '" . (int)$option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ov.sort_order, ovd.name");

		foreach ($option_value_query->rows as $option_value) {
			$option_value_data[] = array(
				'option_value_id' => $option_value['option_value_id'],
				'name'            => $option_value['name'],
				'image'           => $option_value['image'],
				'sort_order'      => $option_value['sort_order']
			);
		}

		return $option_value_data;
	}

  	public function getProductNameByID($product_id){
  		$query = $this->db->query("SELECT name as product_name FROM  `". DB_PREFIX ."product_description` WHERE product_id=".$product_id)->row;

		return $query['product_name'];
  	}

  	public function getManufacNameByID($manufacturer_id){
  		$query = $this->db->query("SELECT name as manufacturer_name FROM  `". DB_PREFIX ."manufacturer` WHERE manufacturer_id=".$manufacturer_id)->row;

		return $query['manufacturer_name'];
  	}

  	public function getCustomerNameByID($customer_id) {
		$query = $this->db->query("SELECT CONCAT(firstname, ' ' ,lastname) AS customer_name FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'")->row;

		return $query['customer_name'];
	}

	public function getAllPromos(){
		$this->config->load('isenselabs/promotion');


		$query = $this->db->query("SELECT setting,module_id FROM " . DB_PREFIX . "module WHERE code = 'promotion' ");
		$promotions = array();
        foreach ($query->rows as $key => $value) {

        	if(version_compare(VERSION, '2.1.0.0', '<')) {
        		$promotions[$key] = unserialize($value['setting']);
        	} else {
        		$promotions[$key] = json_decode($value['setting'], true);
        	}

           	
           	$promotions[$key]['module_id'] = $value['module_id'];
            $promotions[$key]['link'] = $this->url->link($this->config->get('promotion_modulePath').'/promotion', 'module_id='.$value['module_id'].'&' . $this->config->get('promotion_token') . '=' . $this->session->data[$this->config->get('promotion_token')], 'SSL');
            $promotions[$key]['delete_link'] = $this->url->link($this->config->get('promotion_modulePath').'/delete', 'module_id='.$value['module_id'].'&' . $this->config->get('promotion_token') . '=' . $this->session->data[$this->config->get('promotion_token')], 'SSL');
            $promotions[$key]['uses'] = $this->getTotalUses($value['module_id']);
        }

        usort($promotions, function($a, $b) {
        	$a1 = ($a['status'] == 'yes') ? 1 : 0;
        	$b2 = ($b['status'] == 'yes') ? 1 : 0;
		    return  $b2 - $a1 ;
		});

        return $promotions;
	}

	public function getTotalUses($promotion_id){
		$query = $this->db->query("SELECT customer_id FROM `" . DB_PREFIX . "promotion` WHERE promotion_id=".(int)$promotion_id)->rows;
	
		$uses['guests'] = 0;
		$uses['customers'] = 0;
		foreach ($query as $value) {
			if($value['customer_id'] == 0) $uses['guests']++;
			else $uses['customers']++;
		}
		$uses['total'] = $uses['guests']+$uses['customers'];

		return $uses;
		
	}

	public function changeStatus($module_id, $status) {
		$this->load->model('setting/module');
		$module_info = $this->model_setting_module->getModule($module_id);

		$module_info['status'] = $status;

		$this->model_setting_module->editModule($module_id, $module_info);
		
	}
	
}