<?php
class ModelExtensionShortpanel extends Model {
	
	 public function install() {        
			$query = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "product_description LIKE 'short_description'");			
			if($query->num_rows < 1) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "product_description` ADD `short_description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `description`;");				
				}
			}
			
	 public function uninstall() {				
		 $this->db->query("ALTER TABLE `" . DB_PREFIX . "product_description` DROP COLUMN `short_description`");			
			}	
		}
?>