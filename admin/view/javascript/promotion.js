$(document).ready(function(e){
	$('body').on('click', function(){
		$('ul.dropdown-menu').hide();
	});
})

function tour_info(heading, text, element, placement)
{
	placement = placement || 'right';
	var  html_template = '<div style="min-width:350px;" class="popover panel panel-default"><div class="arrow"></div>';
		 html_template += '<div class="panel-heading">'+heading;
		 html_template += '<button type="button" style="padding:1px 4px;" class="btn btn-danger popover-close pull-right"><i style="font-size:11px;" class="fa fa-times"></i></button></div>';

		 html_template += '<div class="panel-body"><div style="min-height:100px;" class="tour-content">'+text+'</div>';

		 html_template += '<button onclick="next_tour();" type="button" style="margin-left:5px;" class="btn btn-primary popover-next pull-right">Next&nbsp;<i class="fa fa-arrow-right"></i></button>';
		 html_template += '<button onclick="prev_tour();" type="button"  style="margin-left:5px;" class="btn btn-primary popover-previous pull-right"><i class="fa fa-arrow-left"></i>&nbsp;Previous</button>';

		 html_template += '</div>';
		 html_template += '</div>';

	$(element).popover({

	    placement : placement,
	    html: 'true',
	    content: text,
	    template: html_template,
	    trigger: 'manual'
	});

	$('.popover').remove();

    $(element).popover("show");

	$(document).on("click", ".popover-close" , function(){
        $(this).parents(".popover").popover('hide');
    });

    return;
}

var curr_popover = -1;
var data_tour;
function start_tour(data_tourr) {
	data_tour = data_tourr;

	$('.popover').remove();
	curr_popover = 0;

	tour_info(data_tour[curr_popover]['title'],data_tour[curr_popover]['text'],data_tour[curr_popover]['selector'],data_tour[curr_popover]['placement']);

	if (data_tour[curr_popover]['onInit']) {
		data_tour[curr_popover]['onInit']();
	}
}

function next_tour(skip){
	skip = skip || "default";

	$('.popover').remove();

	if (data_tour[curr_popover]['onExit']) {
		data_tour[curr_popover]['onExit']();
	}

	if(skip == "default"){
		if (data_tour[curr_popover]['onNextElement']) {
			data_tour[curr_popover]['onNextElement']();
		}
	}

	curr_popover+=1;
	if (data_tour[curr_popover]) {
		tour_info(data_tour[curr_popover]['title'],data_tour[curr_popover]['text'],data_tour[curr_popover]['selector']);

		if (data_tour[curr_popover]['onInit']) {
			data_tour[curr_popover]['onInit']();
		}
	}

}



function prev_tour() {
	$('.popover').remove();

	curr_popover-=1;

	if (data_tour[curr_popover]['onBackElement']) {
		data_tour[curr_popover]['onBackElement']();
	}

	if (data_tour[curr_popover]) {
		tour_info(data_tour[curr_popover]['title'],data_tour[curr_popover]['text'],data_tour[curr_popover]['selector']);

		if (data_tour[curr_popover]['onInit']) {
			data_tour[curr_popover]['onInit']();
		}
	}
}

//Promotions

var data_tour_flat_disc_on_cat = [
	{
		title: 'Discount on a selected category',
		text: 'Let’s start. First, we have to go to the Conditions tab to setup the conditions.',
		selector: 'a[href="#tab-conditions"]',
		placement: 'bottom',
		onInit: function() {
			$('#conditions_group').html('');
			$('.popover-previous').text('Cancel');
		},
		onNextElement: function() {
			$('a[href="#tab-conditions"]').click();
		},
		onBackElement: function() {
			$('#conditions_group').html('');
		}
	},
	{
		title: 'Discount on a selected category - step 1',
		text: 'Second, add a new condition or click next and the tutorial will add it for you.',
		selector: '#addnewcond',
		onInit: function() {
			$('a[href="#tab-conditions"]').click();

			$( "#addnewcond" ).bind( "click.addnewcond", function() {
			  	next_tour(1);
			});
		},
		onNextElement: function() {
			$('#addnewcond').click();
		},
		onBackElement: function() {
			$('#conditions_group > div:last').remove()
		},
		onExit: function() {
			$( "#addnewcond" ).unbind( "click.addnewcond" );
		},
	},
	{
		title: 'Discount on a selected category - step 2',
		text: 'Good job. Now, select "Product from category in cart" option or click next and the tour will select it for you.',
		selector: '#conditions_group > div:last [id^="mainSelect"]',
		onInit: function() {
			$('#conditions_group > div:last [id^="mainSelect"]').bind("change.catname", function() {
				if( $('#conditions_group > div:last [id^="mainSelect"] option:selected').val() == 'category_name') {
					next_tour(1);
				}
			});
		},
		onNextElement: function() {

			var id = $('#conditions_group > div:last').attr('data-id');

			$('#conditions_group > div:last [id^="mainSelect"] option').filter(function() {
			 return ($(this).val() == 'category_name');
			}).prop('selected', true);

            getFields($('#conditions_group > div:last').find('#mainSelect'+id));

		},
		onBackElement: function() {
			$('#conditions_group > div:last [id^="mainSelect"] option:first').prop('selected', true);
		},
		onExit: function() {
			$('#conditions_group > div:last [id^="mainSelect"]').unbind("change.catname");
		},
	},
	{
		title: 'Discount on a selected category - step 3',
		text: 'Enter the desired category name for this promotion',
		selector: '#conditions_group > div:last [class="category_name condition"] >  span:first'
	},
	{
		title: 'Discount on a selected category - step 4',
		text: 'Enter the minimum condition quantity.',
		selector: '#conditions_group > div:last [class="category_name condition"] >  span:last',
		onNextElement: function() {
			$('#getMainSelect1 option:first').prop('selected', true);
			get_cond_getField(1);

			$('a[href="#tab-actions"]').click();
		},
		onBackElement: function() {
			$('a[href="#tab-conditions"]').click();
		},
	},
	{
		title: 'Discount on a selected category - step 5',
		text: 'Choose the <strong>Discount each condition products (if any).</strong> option or click next and the tour will select that for you.',
		selector: '#getMainSelect1',
		onInit: function() {
			$('#getMainSelect1').bind("change.actionchange", function() {
				if( $('#getMainSelect1 option:selected').val() == 'discount_products_from_conditions') {
					next_tour(1);
				}
			});
		},
		onNextElement: function() {

			$('#getMainSelect1 option').filter(function() {
			 return ($(this).val() == 'discount_products_from_conditions');
			}).prop('selected', true);

			get_cond_getField(1);

		},
		onBackElement: function() {
			$('#getMainSelect1 option:first').prop('selected', true);
			get_cond_getField(1);
		},
		onExit: function() {
			$('#getMainSelect1').unbind("change.actionchange");
		},
	},
	{
		title: 'Buy 1 Get 1 for Free - step 6',
		text: 'Configure the promotion type. Fixed discount means that the discount applied to the products will be a fixed number. Fixed price will discount the product to the price.',
		selector: '#discount_type'
	},
	{
		title: 'Buy 1 Get 1 for Free - final step',
		text: 'Enter the amount of discount you want to apply to each product.',
		selector: '#input-amount',
		onInit: function() {
			$('.popover-next').text('Finish');
		}
	},
];

var data_free_shipping_on_orders = [
	{
		title: 'Discount on a selected category',
		text: 'Let’s start. First, we have to go to the Conditions tab to setup the conditions.',
		selector: 'a[href="#tab-conditions"]',
		placement: 'bottom',
		onInit: function() {
			$('#conditions_group').html('');
			$('.popover-previous').text('Cancel');
		},
		onNextElement: function() {
			$('a[href="#tab-conditions"]').click();
		},
		onBackElement: function() {
			$('#conditions_group').html('');
		}
	},
	{
		title: 'Free shipping on orders over/under/equal X - step 1',
		text: 'Second, add a new condition or click next and the tutorial will add it for you.',
		selector: '#addnewcond',
		onInit: function() {
			$('a[href="#tab-conditions"]').click();

			$( "#addnewcond" ).bind( "click.addnewcond", function() {
			  	next_tour(1);
			});
		},
		onNextElement: function() {
			$('#addnewcond').click();
		},
		onBackElement: function() {
			$('#conditions_group > div:last').remove()
		},
		onExit: function() {
			$( "#addnewcond" ).unbind( "click.addnewcond" );
		},
	},
	{
		title: 'Free shipping on orders over/under/equal X - step 2',
		text: 'Good job. Now, select "Cart total" option or click next and the tour will select it for you.',
		selector: '#conditions_group > div:last [id^="mainSelect"]',
		onInit: function() {
			$('#conditions_group > div:last [id^="mainSelect"]').bind("change.carttotal", function() {
				if( $('#conditions_group > div:last [id^="mainSelect"] option:selected').val() == 'cart_total') {
					next_tour(1);
				}
			});
		},
		onNextElement: function() {

			var id = $('#conditions_group > div:last').attr('data-id');

			$('#conditions_group > div:last [id^="mainSelect"] option').filter(function() {
			 return ($(this).val() == 'cart_total');
			}).prop('selected', true);

            getFields($('#conditions_group > div:last').find('#mainSelect'+id));

		},
		onBackElement: function() {
			$('#conditions_group > div:last [id^="mainSelect"] option:first').prop('selected', true);
		},
		onExit: function() {
			$('#conditions_group > div:last [id^="mainSelect"]').unbind("change.carttotal");
		},
	},
	{
		title: 'Free shipping on orders over/under/equal X - step 3',
		text: 'Choose the whether the promotion will be on orders over/under or equal to X.',
		selector: '#conditions_group > div:last [id^="equal-input"]'
	},
	{
		title: 'Free shipping on orders over/under/equal X - step 4',
		text: 'Type the desired cart total value.',
		selector: '#conditions_group > div:last [id^="condition-input"]',
		onNextElement: function() {
			$('#getMainSelect1 option:first').prop('selected', true);
			get_cond_getField(1);

			$('a[href="#tab-actions"]').click();
		},
		onBackElement: function() {
			$('a[href="#tab-conditions"]').click();
		},
	},
	{
		title: 'Free shipping on orders over/under/equal X - step 5',
		text: 'Choose the <strong>Free shipping</strong> option or click next and the tour will select that for you.',
		selector: '#getMainSelect1',
		onInit: function() {
			$('#getMainSelect1').bind("change.actionchange", function() {
				if( $('#getMainSelect1 option:selected').val() == 'free_shipping') {
					next_tour(1);
				}
			});
		},
		onNextElement: function() {

			$('#getMainSelect1 option').filter(function() {
			 return ($(this).val() == 'free_shipping');
			}).prop('selected', true);

			get_cond_getField(1);

		},
		onBackElement: function() {
			$('#getMainSelect1 option:first').prop('selected', true);
			get_cond_getField(1);
		},
		onExit: function() {
			$('#getMainSelect1').unbind("change.actionchange");
		},
	},
	{
		title: 'Free shipping on orders over/under/equal X - final step',
		text: 'Well done. You have set up the promotion conditions and actions, go and set up the general settings if you haven`t.',
		selector: '#getMainSelect1',
		onInit: function() {
			$('.popover-next').text('Finish');
		}
	}
];

var data_tour_bogof = [
	{
		title: 'Discount on a selected category',
		text: 'Let’s start. First, we have to go to the Conditions tab to setup the conditions.',
		selector: 'a[href="#tab-conditions"]',
		placement: 'bottom',
		onInit: function() {
			$('#conditions_group').html('');
			$('.popover-previous').text('Cancel');
		},
		onNextElement: function() {
			$('a[href="#tab-conditions"]').click();
		},
		onBackElement: function() {
			$('#conditions_group').html('');
		}
	},
	{
		title: 'Buy 1 Get 1 For Free - step 1',
		text: 'Second, add a new condition or click next and the tutorial will add it for you.',
		selector: '#addnewcond',
		onInit: function() {
			$('a[href="#tab-conditions"]').click();

			$( "#addnewcond" ).bind( "click.addnewcond", function() {
			  	next_tour(1);
			});
		},
		onNextElement: function() {
			$('#addnewcond').click();
		},
		onBackElement: function() {
			$('#conditions_group > div:last').remove()
		},
		onExit: function() {
			$( "#addnewcond" ).unbind( "click.addnewcond" );
		},
	},
	{
		title: 'Buy 1 Get 1 For Free - step 2',
		text: 'Good job. Now, select the "Product in Cart" option or click next and the tutorial will select it for you.',
		selector: '#conditions_group > div:last [id^="mainSelect"]',
		onInit: function() {
			$('#conditions_group > div:last [id^="mainSelect"]').bind("change.productname", function() {
				if( $('#conditions_group > div:last [id^="mainSelect"] option:selected').val() == 'product_name') {
					next_tour(1);
				}
			});
		},
		onNextElement: function() {

			var id = $('#conditions_group > div:last').attr('data-id');

			$('#conditions_group > div:last [id^="mainSelect"] option').filter(function() {
			 return ($(this).val() == 'product_name');
			}).prop('selected', true);

			getFields($('#conditions_group > div:last').find('#mainSelect'+id));

		},
		onBackElement: function() {
			$('#conditions_group > div:last [id^="mainSelect"] option:first').prop('selected', true);
		},
		onExit: function() {
			$('#conditions_group > div:last [id^="mainSelect"]').unbind("change.productname");
		},
	},
	{
		title: 'Buy 1 Get 1 For Free - step 3',
		text: 'Enter the desired product name for this promotion',
		selector: '#conditions_group > div:last [class="product_name condition"] >  span:first'
	},
	{
		title: 'Buy 1 Get 1 for Free - step 4',
		text: 'Specifically for Buy 1 Get 1 For Free, the minimum quantity should be 1. However, if you want to make the promotion Buy X get Y, change this field to the required minimum quantity.',
		selector: '#conditions_group > div:last [class="product_name condition"] >  span:last',
		onNextElement: function() {
			$('#getMainSelect1 option:first').prop('selected', true);
			get_cond_getField(1);

			$('a[href="#tab-actions"]').click();
		},
		onBackElement: function() {
			$('a[href="#tab-conditions"]').click();
		},
	},
	{
		title: 'Buy 1 Get 1 for Free - step 5',
		text: 'Choose the <strong>Discount on products</strong> option or click next and the tour will select that for you.',
		selector: '#getMainSelect1',
		onInit: function() {
			$('#getMainSelect1').bind("change.actionchange", function() {
				if( $('#getMainSelect1 option:selected').val() == 'discount_on_products') {
					next_tour(1);
				}
			});
		},
		onNextElement: function() {

			$('#getMainSelect1 option').filter(function() {
			 return ($(this).val() == 'discount_on_products');
			}).prop('selected', true);

			get_cond_getField(1);

		},
		onBackElement: function() {
			$('#getMainSelect1 option:first').prop('selected', true);
			get_cond_getField(1);
		},
		onExit: function() {
			$('#getMainSelect1').unbind("change.actionchange");
		},
	},
	{
		title: 'Buy 1 Get 1 for Free - step 6',
		text: 'Configure the promotion type. Fixed discount means that the discount applied to the products will be a fixed number. Fixed price will discount the product to the price.  For Buy 1 Get 1 for Free, you should select the percentage type.',
		selector: '#discount_type'
	},
	{
		title: 'Buy 1 Get 1 for Free - step 7',
		text: 'Enter the amount of discount you want to apply to each product.  For Buy 1 Get 1 for Free, you should enter a %100, because the promotion will discount one item at full price.',
		selector: '#input-amount'
	},
	{
		title: 'Buy 1 Get 1 for Free - final step',
		text: 'Here you can enter a product name, which will be discounted with the selected settings above.',
		selector: '#input-get-product',
		onInit: function() {
			$('.popover-next').text('Finish');
		}
	}
];
