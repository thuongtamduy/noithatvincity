<?php

$_['heading_title']     	= '<span style="color: #CC0000; font-weight: bold">MD:</span> Product Short Description';
$_['edit_title']       		= 'Edit Product Short Description';
$_['text_extension']   		= 'Extensions';
$_['text_success']       	= 'Success: You have modified the Product Short Description module';
$_['button_stay']       	= 'Save & Stay';
$_['button_support']        = 'New Ticket';
$_['support_url'] 			= 'http://support.mikadesign.co.uk/?extension=Product Short Description Journal OC3x - Question';

// Entry
$_['entry_status']			= 'Status';
$_['entry_position']		= 'Position';
$_['entry_css']				= 'Custom CSS';
$_['entry_content_css'] 	= 'Custom CSS Value';
$_['entry_sn']				= 'Rich Text Editor';
$_['entry_ellipsis']		= 'Ellipsis (...)';
$_['entry_product'] 		= 'Product Page';
$_['entry_related'] 		= 'Related Products';
$_['entry_bestseller']     	= 'Bestseller Module';
$_['entry_featured']     	= 'Featured Module';
$_['entry_latest']     		= 'Latest Module';
$_['entry_special']     	= 'Specials Module';
$_['entry_category']   		= 'Product Listing';
$_['entry_manufacturer']   	= 'Manufacturers Listing';
$_['entry_search']  		= 'Search Listing';
$_['entry_special2']   		= 'Specials Listing';
$_['entry_compare']    		= 'Compare Page';
$_['entry_carousel']     	= 'Carousel Module';
$_['entry_sections']		= 'Custom Sections Module';
$_['entry_card']     		= 'Product Card';
$_['entry_side']			= 'Side Products';
$_['entry_product_name']	= 'Product Name';
$_['entry_doc']				= 'Documentation';
$_['entry_support']			= 'Support';
$_['entry_market']			= 'Our Extensions';
$_['entry_replace']			= 'Replace Description';

// Tab
$_['tab_settings']          = 'Settings';
$_['tab_options']           = 'Options';
$_['tab_css']               = 'Custom CSS';
$_['tab_ckeditor']          = 'CkEditor';
$_['tab_about']             = 'About & Support';

// Text
$_['text_product_page']         = 'This is for the Product Page Only';
$_['text_position']			    = 'Choose the Short Description Position';
$_['text_below_name']           = 'Show Short Description Below the Product Name';
$_['text_above_price']          = 'Show Short Description Above the Product Price';
$_['text_below_price']          = 'Show Short Description Below the Product Price';
$_['text_product']				= 'Product Short Description';
$_['text_doc']					= 'Product Short Description 3x Documentation';
$_['text_support']				= 'CliCK Here for Support';
$_['text_market']				= 'OpenCart Marketplace';
$_['text_success'] 				= 'Success: You have modified the Product Short Description module';
$_['text_journal2']     		= 'For Journal 2 Theme Only';
$_['text_journal3']     		= 'For Journal 3 Theme Only';
$_['text_settings_maxlength'] 	= 'Short Description Limit';
$_['text_help_status'] 			= 'Enable for Product Short Description';
$_['text_help_sn'] 			    = 'Enable for Summernote Rich Text Editor';
$_['text_help_content_css'] 	= 'Use plain CSS without tags';
$_['text_help_field']  			= 'Select Product Short Description, you can choose any of the following three positions, below the product name, above the product price or below the product price on the product page only';
$_['text_help_length']  		= 'In the list view, short description character limit (categories, special etc)';
$_['text_help_replace'] 		= 'Turn on to Replace the Description on the Category, Manufacturer, Search, Special, Related Products and Product Compare pages, Bestseller, Featured, Latest and Special modules<br>* This is for Journal 2 only *';
$_['text_help_ellipsis'] 	 	= 'The Ellipsis (...) property specifies how overflowed content that is not displayed should be signaled to the user. Disable this if you are using Images or you are showing the full Short Description in the product listings, categories, special etc';
$_['text_help_css']    			= 'Just enable and edit the Custom CSS or add your own Custom CSS in the field below. Alternatively if you dont want to use this Custom CSS, just leave it disabled.';

$_['error_permission']    		= 'Warning: You do not have permission to modify the Product Short Description module, cheCK the permissions under System > Users > User Groups ';