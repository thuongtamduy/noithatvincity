<?php
// Heading
$_['heading_title']    = 'Promotion';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Success: You have modified promotion total!';
$_['text_edit']        = 'Edit Promotion Total';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Promotion total!';