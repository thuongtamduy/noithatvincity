<?php

use Journal3\Opencart\ModuleController;
use Journal3\Options\Parser;
use Journal3\Utils\Arr;

class ControllerJournal3Blocks extends ModuleController {

	private static $PRODUCT_INFO;

	public function __construct($registry) {
		parent::__construct($registry);
		$this->load->model('journal3/links');
	}

	public function index($args) {
		$data = parent::index($args);

		if (!$data) {
			return null;
		}

		if ($this->settings['carousel']) {
			$this->journal3->document->addStyle('catalog/view/theme/journal3/lib/swiper/swiper.min.css');
			$this->journal3->document->addScript('catalog/view/theme/journal3/lib/swiper/swiper.min.js', 'footer');
		}

		return $data;
	}

	/**
	 * @param Parser $parser
	 * @param $index
	 * @return array
	 */
	protected function parseGeneralSettings($parser, $index) {
		$default = $parser->getSetting('default');

		$data = array(
			'classes'         => array(
				'blocks-' . $parser->getSetting('display'),
				'carousel-mode' => $parser->getSetting('carousel'),
			),
			'carouselOptions' => $this->journal3->carousel($parser->getJs(), 'carouselStyle'),
		);

		$data['default_index'] = $parser->getSetting('display') === 'tabs' ? 1 : 0;

		if ($default) {
			foreach (Arr::get($this->module_data, 'items') as $index => $item) {
				if ($default === Arr::get($item, 'id')) {
					$data['default_index'] = $index + 1;
					break;
				}
			}
		}

		return $data;
	}

	/**
	 * @param Parser $parser
	 * @param $index
	 * @return array
	 */
	protected function parseItemSettings($parser, $index) {
		$title = $parser->getSetting('title');

		switch ($parser->getSetting('contentType')) {
			case 'description':
			case 'attributes':
			case 'reviews':
			case 'dynamic':
				$content = '';
				break;

			default:
				$content = $parser->getSetting('content');
		}

		return array(
			'tab_classes'   => array(
				'tab-' . $this->item_id,
				'active' => ($this->settings['display'] === 'tabs') && ($index === $this->settings['default_index']),
			),
			'panel_classes' => array(
				'panel-collapse',
				'collapse',
				'in' => ($this->settings['display'] === 'accordion') && ($index === $this->settings['default_index']),
			),
			'classes'       => array(
				'tab-pane'     => $this->settings['display'] === 'tabs',
				'active'       => ($this->settings['display'] === 'tabs') && ($index === $this->settings['default_index']),
				'panel'        => $this->settings['display'] === 'accordion',
				'panel-active' => ($this->settings['display'] === 'accordion') && ($index === $this->settings['default_index']),
				'swiper-slide' => ($this->settings['display'] === 'grid') && $this->settings['carousel'],
			),
			'image'         => $this->model_journal3_image->resize($parser->getSetting('image'), $this->settings['imageDimensions']['width'], $this->settings['imageDimensions']['height'], $this->settings['imageDimensions']['resize']),
			'image2x'       => $this->model_journal3_image->resize($parser->getSetting('image'), $this->settings['imageDimensions']['width'] * 2, $this->settings['imageDimensions']['height'] * 2, $this->settings['imageDimensions']['resize']),
			'title'         => $title,
			'content'       => $content,
		);
	}

	/**
	 * @param Parser $parser
	 * @param $index
	 * @return array
	 */
	protected function parseSubitemSettings($parser, $index) {
		return array();
	}

	protected function beforeRender() {
		foreach ($this->settings['items'] as $key => &$item) {
			// product tabs
			if (isset($this->request->get['product_id'])) {
				if (in_array($item['contentType'], array('description', 'attributes', 'reviews'))) {
					$item['content'] = $this->content($item['contentType']);
				}
			}

			// dynamic
			if ($item['contentType'] === 'dynamic' && $item['dynamic']) {
				$item['content'] = $this->load->controller($item['dynamic'], array(
					'module_id' => $this->module_id,
					'item'      => $item,
					'settings'  => $this->settings,
				));
			}

			$limit = (int)$this->journal3->settings->get('globalExpandCharactersLimit');

			if ($limit > 0 && (utf8_strlen(strip_tags($item['content'])) <= $limit)) {
				$item['classes'][] = 'no-expand';
			}

			if (!$item['content']) {
				// force update default_index before render if current item is default
				if ($key === $this->settings['default_index']) {
					$this->settings['default_index'] = -1;
				}

				unset($this->settings['items'][$key]);

				continue;
			}
		}

		if (!$this->settings['items']) {
			$this->settings = null;

			return;
		}

		if ($this->settings['display'] === 'tabs') {
			if ($this->settings['default_index'] === -1) {
				reset($this->settings['items']);
				$key = key($this->settings['items']);

				$this->settings['items'][$key]['tab_classes'][] = 'active';
				$this->settings['items'][$key]['classes'][] = 'active';
			}
		}

		if ($this->settings['display'] === 'accordion') {
			if ($this->settings['default_index'] === -1) {
				reset($this->settings['items']);
				$key = key($this->settings['items']);

				$this->settings['items'][$key]['panel_classes'][] = 'in';
				$this->settings['items'][$key]['classes'][] = 'active';
			}
		}
	}

	private function content($type) {
		if (static::$PRODUCT_INFO === null) {
			$product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);

			// desc
			static::$PRODUCT_INFO['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');

			if (!trim(strip_tags(static::$PRODUCT_INFO['description']))) {
				static::$PRODUCT_INFO['description'] = '';
			}

			// attrs
			$data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);
			static::$PRODUCT_INFO['attributes'] = $this->renderView('journal3/module/product_blocks_attributes', $data);

			// reviews
			$this->load->language('product/product');

			$data['text_write'] = $this->language->get('text_write');
			$data['entry_name'] = $this->language->get('entry_name');
			$data['entry_review'] = $this->language->get('entry_review');
			$data['text_note'] = $this->language->get('text_note');
			$data['entry_rating'] = $this->language->get('entry_rating');
			$data['entry_bad'] = $this->language->get('entry_bad');
			$data['entry_good'] = $this->language->get('entry_good');
			$data['text_loading'] = $this->language->get('text_loading');
			$data['button_continue'] = $this->language->get('button_continue');

			$data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true));
			$data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);

			$data['review_status'] = $this->config->get('config_review_status');

			if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
				$data['review_guest'] = true;
			} else {
				$data['review_guest'] = false;
			}

			if ($this->customer->isLogged()) {
				$data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
			} else {
				$data['customer_name'] = '';
			}

			$data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
			$data['rating'] = (int)$product_info['rating'];

			// Captcha
			if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'));
			} else {
				$data['captcha'] = '';
			}

			static::$PRODUCT_INFO['reviews'] = $this->renderView('journal3/module/product_blocks_reviews', $data);
		}

		return static::$PRODUCT_INFO[$type];
	}

}
