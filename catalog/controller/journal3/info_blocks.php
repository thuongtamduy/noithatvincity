<?php

use Journal3\Opencart\ModuleController;
use Journal3\Options\Parser;

class ControllerJournal3InfoBlocks extends ModuleController {

	public function __construct($registry) {
		parent::__construct($registry);
	}

	/**
	 * @param Parser $parser
	 * @param $module_id
	 * @return array
	 */
	protected function parseGeneralSettings($parser, $module_id) {
		return array();
	}

	/**
	 * @param Parser $parser
	 * @param $index
	 * @return array
	 */
	protected function parseItemSettings($parser, $index) {
		$data = array(
			'classes' => array(
				'info-blocks',
			),
		);

		return $data;
	}

	/**
	 * @param Parser $parser
	 * @param $index
	 * @return array
	 */
	protected function parseSubitemSettings($parser, $index) {
		return array();
	}

}
