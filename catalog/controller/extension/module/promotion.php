<?php
/**
* 
*/
class ControllerExtensionModulePromotion extends Controller
{
	private $moduleModel;
    private $moduleName;
    private $modulePath;
    private $callModel;
    private $moduleVersion;

	public function __construct($registry)
	{
		parent::__construct($registry);	

		$this->config->load('isenselabs/promotion');

		$this->moduleName = $this->config->get('promotion_moduleName');
        $this->modulePath = $this->config->get('promotion_modulePath');
        $this->moduleVersion = $this->config->get('promotion_moduleVersion');

        $this->load->model($this->modulePath);

        $this->callModel = $this->config->get('promotion_callModel');
        $this->moduleModel = $this->{$this->callModel};

		$this->load->model('tool/image');


	}
	
	public function index() {

		$this->load->language($this->modulePath);

		$promotions = $this->moduleModel->getPromotions();

		foreach ($promotions as &$promo) {
            $content =  strip_tags(html_entity_decode($promo['information_page']['description'][$this->config->get('config_language')]));
            $promo['content'] = (strlen($content) > 100) ? substr($content, 0, 100) . '..' : $content;
		}

		$data['chunked_promotions'] = array_chunk($promotions, 4);

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link($this->modulePath, '', 'SSL')
		);

		$language_strings = $this->load->language($this->modulePath);
		foreach ($language_strings as $key => $value) {
			$data[$key] = $value;
		}

		$data['current_language'] = $this->config->get('config_language');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if(version_compare(VERSION, '2.2.0.0', '<')) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/'.$this->modulePath.'/index.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/'.$this->modulePath.'/index.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/'.$this->modulePath.'/index.tpl', $data));
			}
		} else {
			$this->response->setOutput($this->load->view($this->modulePath.'/index', $data));
		}
	}

	private function getImageConfigs($name) {
		if(version_compare(VERSION, '2.2.0.0', '<')) {
			return $this->config->get($name);
		} else {
			if(strpos($name, 'config_image') !== false){
				if(version_compare(VERSION, '2.3.0.0', '>=')) {
					$name = str_replace('config', '', $name);
					return $this->config->get('theme_'.$this->config->get('config_theme') . $name);
				} else {
					$name = str_replace('config', '', $name);
					return $this->config->get($this->config->get('config_theme') . $name);
				}
			}
		}

        return $this->config->get($name);
	}

	public function view() {

		$this->load->language($this->modulePath);

		$promotion_id = 0;
		if(isset($this->request->get['promotion_id'])) {
			$promotion_id = $this->request->get['promotion_id'];
		} else {
			$this->response->redirect($this->url->link('error/not_found', '', 'SSL'));
		}
		$data['promotion_id'] = $promotion_id;

		$data['text_tax'] = 'tax';

		$promotion_data = $this->moduleModel->getPromotion($promotion_id);
		$data['promotion_data'] = $promotion_data;

		$language_strings = $this->load->language($this->modulePath);
		foreach ($language_strings as $key => $value) {
			$data[$key] = $value;
		}

		$data['promotion_title'] = $promotion_data['name'];
		$data['promotion_description'] = html_entity_decode($promotion_data['information_page']['description'][$this->config->get('config_language')]);

		if(!empty($promotion_data['information_page']['page_title'])) {
			$this->document->setTitle($promotion_data['information_page']['page_title']);
		}
		if(!empty($promotion_data['information_page']['meta_description'])) {
			$this->document->setDescription($promotion_data['information_page']['meta_description']);
		}
		if(!empty($promotion_data['information_page']['meta_keywords'])) {
			$this->document->setKeywords($promotion_data['information_page']['meta_keywords']);
		}
		
		$image_width = 800; $image_height = 500;
		if(isset($promotion_data['information_page']['main_image_width'])) {
			$image_width = $promotion_data['information_page']['main_image_width'];
		}

		if(isset($promotion_data['information_page']['main_image_height'])) {
			$image_height = $promotion_data['information_page']['main_image_height'];
		}

		$image_width_related = $this->getImageConfigs('config_image_related_width');
		$image_height_related = $this->getImageConfigs('config_image_related_height'); 
		
		if(isset($promotion_data['information_page']['small_image_width'])) {
			$image_width_related = $promotion_data['information_page']['small_image_width'];
		}

		if(isset($promotion_data['information_page']['small_image_height'])) {
			$image_height_related = $promotion_data['information_page']['small_image_height'];
		}



		if(isset($promotion_data['information_page']['image']) && !empty($promotion_data['information_page']['image'])){
			$data['promotion_image'] = $this->model_tool_image->resize($promotion_data['information_page']['image'], $image_width, $image_height);
		} else {
			$data['promotion_image'] = '';//$this->model_tool_image->resize('no_image.png', $image_width, $image_height);
		}

		if(isset($data['promotion_data']['included_products']['product_ids'])) {
			$this->load->model('catalog/product');
			foreach ($data['promotion_data']['included_products']['product_ids'] as &$product_id) {

				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $image_width_related, $image_height_related);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $image_width_related, $image_height_related);
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}

					$data_temp = array(
						'product_id'  => $product_info['product_id'],
						'thumb'       => $image,
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}

				$product_id = $data_temp;
			}
		}

		if(isset($data['promotion_data']['included_products']['category_ids'])) {
			$this->load->model('catalog/category');
			foreach ($data['promotion_data']['included_products']['category_ids'] as &$category_id) {
				$category_info = $this->model_catalog_category->getCategory($category_id);

				if ($category_info) {
					if ($category_info['image']) {
						$image = $this->model_tool_image->resize($category_info['image'], $image_width_related, $image_height_related);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $image_width_related, $image_height_related);
					}

					$data_temp = array(
						'product_id'  => $category_info['category_id'],
						'thumb'       => $image,
						'name'        => $category_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
						'href'        => $this->url->link('product/category', 'path=' . $category_info['category_id'])
					);
				}


				$category_id = $data_temp;
			}
		}

		if(isset($data['promotion_data']['included_products']['manufacturer_ids'])) {
			$this->load->model('catalog/manufacturer');
			foreach ($data['promotion_data']['included_products']['manufacturer_ids'] as &$manufacturer_id) {
				$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);

				if ($manufacturer_info) {
					if ($manufacturer_info['image']) {
						$image = $this->model_tool_image->resize($manufacturer_info['image'], $image_width_related, $image_height_related);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $image_width_related, $image_height_related);
					}

					$data_temp = array(
						'product_id'  => $manufacturer_info['manufacturer_id'],
						'thumb'       => $image,
						'name'        => $manufacturer_info['name'],
						'href'        => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer_info['manufacturer_id'])
					);
				}



				$manufacturer_id = $data_temp;
			}
		}



		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link($this->modulePath, '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $promotion_data['name'],
			'href' => $this->url->link($this->modulePath, 'promotion_id='.$promotion_id, 'SSL')
		);

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$data['current_language'] = $this->config->get('config_language');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if(version_compare(VERSION, '2.2.0.0', '<')) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/'.$this->modulePath.'/view.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/'.$this->modulePath.'/view.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/'.$this->modulePath.'/view.tpl', $data));
			}
		} else {
				$this->response->setOutput($this->load->view($this->modulePath.'/view', $data));
		}

	}

	    // catalog/controller/extension/total/coupon/coupon/after
    public function checkForCouponUsage(&$route, &$data, &$output) {
        $totalPath = $this->config->get('promotion_totalPath');
        $totalCallModel = $this->config->get('promotion_totalModel');

        $this->load->model($totalPath);
        if($this->{$totalCallModel}->isCouponCodeValid($this->request->post['coupon'])){
            $this->session->data['coupon_promotion_module'] = $this->request->post['coupon'];
            $this->session->data['success'] = "You applied a promotion coupon code successfully.";
            $json['redirect'] = $this->url->link('checkout/cart');
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    // model/checkout/order/addOrderHistory/after
    public function savePromotionUse(&$route, &$args, &$output) {

        $order_id = $args[0];
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($order_id);
        // Fraud Detection
        $this->load->model('account/customer');

        $customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);


        if(isset($this->session->data['promotions'])) {
            foreach ($this->session->data['promotions'] as $promotion_id) {

                $customer_id = ($customer_info && $customer_info['safe']) ? $order_info['customer_id']  : 0;
                $coupon_code = (isset($this->session->data['coupon_promotion_module'])) ? $this->session->data['coupon_promotion_module'] : '' ;

                $this->db->query("INSERT INTO `" . DB_PREFIX . "promotion`
                    SET
                        promotion_id 	= '" . $this->db->escape($promotion_id) . "',
                        customer_id 	= '" . $this->db->escape($customer_id) . "',
                        order_id    	= '" . $this->db->escape($order_id) . "',
                        time 			=  NOW(),
                        coupon_code 	= '" . $this->db->escape($coupon_code) . "'
                ");
            }

            unset($this->session->data['promotions']);
        }
    }

    // catalog/view/product/product/after
    public function attachPromotionTabsAndButton(&$route, &$data, &$output) {

    	$totalPath = $this->config->get('promotion_totalPath');
        $totalCallModel = $this->config->get('promotion_totalModel');
        $languagePath = $this->config->get('promotion_modulePath');

        $this->load->model($totalPath);
        $promos = $this->{$totalCallModel}->getPromotionsFromProductID($data['product_id']);

        $language_data_promotion = $this->load->language($languagePath);
        foreach ($language_data_promotion as $key => $lang_data) {
            $data[$key] = $lang_data;
        }

        $data['promotions'] = $promos['promotions'];
        $data['promo_button'] = $promos['button'];

        if(!empty($data['promotions'])) {
            $script = $this->load->view($this->modulePath . "/events/addTabsAndButton", $data);
            $output = $output . $script;
        }
    }

    /*
    * Event for SEO URLs (1/2)
    * catalog/asterisk/before
    */
     public function customUrlFunctionality($eventRoute) {

        $this->event->unregister("*/before", $this->modulePath . "/customUrlFunctionality");

        if ($this->config->get('config_seo_url')) {
			$this->url->addRewrite($this);
		}

        if (isset($this->request->get['_route_'])) {

            $parts = explode('/', $this->request->get['_route_']);

            $promotions = $this->{$this->config->get('promotion_callModel')}->getPromotions();

            $parts = array_filter($parts);

            foreach ($promotions as $promotion_settings) { 
                if($promotion_settings['status'] == 'yes' && isset($promotion_settings['information_page']['status']) && $promotion_settings['information_page']['status'] == 'yes' && !empty($promotion_settings['information_page']['seo_slug'])) {
                    
            		if (count($parts) == 1 && ($parts[0] == $this->config->get('promotion_seo_slug'))) {
	                    $this->request->get['route'] = $this->config->get('promotion_modulePath');
	                    return new Action($this->request->get['route']);
	                }

                    if(count($parts) == 2 && ($parts[0] == $this->config->get('promotion_seo_slug')) && $parts[1] == $promotion_settings['information_page']['seo_slug']) {

                        $this->request->get['promotion_id'] = $promotion_settings['module_id'];
                        $this->request->get['route'] = $this->config->get('promotion_modulePath').'/view';

                        return new Action($this->request->get['route']); 
                    }
                }
            }
        }      
    }
    
    /*
    * Event for SEO URLs (1/2)
    * catalog/asterisk/before
    */
    public function rewrite($link) {
        
        $url_info = parse_url(str_replace('&amp;', '&', $link));

        $url = '';

		$data = array();

        if (!empty($url_info['query'])) {
            parse_str($url_info['query'], $data);

            if (isset($data['route']) && $data['route'] == $this->config->get('promotion_modulePath')) {
                $url .= '/'.$this->config->get('promotion_seo_slug');
            }

            if (isset($data['route']) && isset($data['promotion_id']) && $data['route'] == $this->config->get('promotion_modulePath').'/view') {

               $promotion_settings = $this->{$this->config->get('promotion_callModel')}->getPromotion($data['promotion_id']);

               $promotion_settings = is_array($promotion_settings) ? $promotion_settings : array();

                if (!empty($promotion_settings) && !empty($promotion_settings['information_page']['seo_slug'])) {
                    $url .= '/'. $this->config->get('promotion_seo_slug') . '/'. $promotion_settings['information_page']['seo_slug'];
                    unset($data['promotion_id']);
                }

            }
            
            if ($url) {
                unset($data['route']);

                $query = '';

                if ($data) {
                    foreach ($data as $key => $value) {
                        $query .= '&' . rawurlencode((string)$key) . '=' . rawurlencode((is_array($value) ? http_build_query($value) : (string)$value));
                    }

                    if ($query) {
                        $query = '?' . str_replace('&', '&amp;', trim($query, '&'));
                    }
                }

                return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
            } else {
                return $link;
            }
        } else {
            return $link;
        }
    }


}