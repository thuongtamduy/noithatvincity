<?php

use Journal3\Opencart\Model;
use Journal3\Utils\Arr;

class ModelJournal3Filter extends Model {

	private $config_customer_group_id;
	private $config_language_id;
	private $config_store_id;
	private $config_tax;

	private static $SORT = array(
		'pd.name',
		'p.model',
		'p.quantity',
		'p.price',
		'rating',
		'p.sort_order',
		'p.date_added',
		'random',
		'p.viewed',
		'sales',
	);

	private static $ORDER = array(
		'ASC',
		'DESC',
	);

	private static $filter_data = null;

	public function __construct($registry) {
		parent::__construct($registry);

		$this->config_customer_group_id = $this->customer->isLogged() ? $this->customer->getGroupId() : $this->config->get('config_customer_group_id');
		$this->config_language_id = $this->config->get('config_language_id');
		$this->config_tax = $this->config->get('config_tax');
		$this->config_store_id = $this->config->get('config_store_id');

		$this->load->model('journal3/product');
	}

	public function filterBase($url) {
		$url = parse_url($url);

		if (isset($url['query'])) {
			parse_str($url['query'], $params);

			unset($params['f']);

			$url['query'] = http_build_query($params);
		}

		$scheme = isset($url['scheme']) ? $url['scheme'] . '://' : '';
		$host = isset($url['host']) ? $url['host'] : '';
		$port = isset($url['port']) ? ':' . $url['port'] : '';
		$user = isset($url['user']) ? $url['user'] : '';
		$pass = isset($url['pass']) ? ':' . $url['pass'] : '';
		$pass = ($user || $pass) ? "$pass@" : '';
		$path = isset($url['path']) ? $url['path'] : '';
		$query = isset($url['query']) ? '?' . $url['query'] : '';
		$fragment = isset($url['fragment']) ? '#' . $url['fragment'] : '';

		$url = urldecode("$scheme$user$pass$host$port$path$query$fragment");

		return $url;
	}

	public function parseFilterData($f) {
		$filter_data = array();

		foreach ($f as $k => $v) {
			switch ($k) {
				case 'min';
					$filter_data['price']['min'] = (float)$v;
					break;

				case 'max';
					$filter_data['price']['max'] = (float)$v;
					break;

				case 'c';
					$filter_data['categories'] = explode(',', $v);
					break;

				case 'm';
					$filter_data['manufacturers'] = explode(',', $v);
					break;

				case 'a';
					foreach ($v as $kk => $vv) {
						$filter_data['attributes'][$kk] = explode(',', $vv);
					}
					break;

				case 'o';
					foreach ($v as $kk => $vv) {
						$filter_data['options'][$kk] = explode(',', $vv);
					}
					break;

				case 'f';
					foreach ($v as $kk => $vv) {
						$filter_data['filters'][$kk] = explode(',', $vv);
					}
					break;

				case 'r';
					$filter_data['ratings'] = explode(',', $v);
					break;

				case 't';
					$filter_data['tags'] = explode(',', $v);
					break;

				case 'q';
					$filter_data['availability'] = explode(',', $v);
					break;
			}
		}

		return $filter_data;
	}

	public function setFilterData($filter_data) {
		static::$filter_data = $filter_data;
	}

	public function getFilterData() {
		return static::$filter_data;
	}

	public function hasFilterData($key, $value) {
		$values = Arr::get(static::$filter_data, $key);

		if (!is_array($values)) {
			return $values === $value;
		}

		return in_array($value, $values);
	}

	public function getPriceRange() {
		$discount = "
			(
				SELECT price 
				FROM `" . DB_PREFIX . "product_discount` pd2 
				WHERE 
					pd2.product_id = p.product_id 
					AND pd2.customer_group_id = '" . (int)$this->config_customer_group_id . "' 
					AND pd2.quantity = '1' 
					AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) 
					AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) 
				ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1
			)
		";

		$special = "
			(
				SELECT price 
				FROM `" . DB_PREFIX . "product_special` ps 
				WHERE 
					ps.product_id = p.product_id 
					AND ps.customer_group_id = '" . (int)$this->config_customer_group_id . "' 
					AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) 
					AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) 
				ORDER BY ps.priority ASC, ps.price ASC LIMIT 1
			)
		";

		$sql = "
			SELECT
				MIN(COALESCE(" . $special . ", " . $discount . ", p.price)) AS min,
				MAX(COALESCE(" . $special . ", " . $discount . ", p.price)) AS max
			FROM `" . DB_PREFIX . "product` p			
		";

		$sql .= $this->addFilters(static::$filter_data, 'price');

		$row = $this->dbQuery($sql, 'PRICE')->row;

		return array(
			'min' => floor($this->applyTax($row['min'])),
			'max' => ceil($this->applyTax($row['max'])),
		);
	}

	public function getCategories() {
		$sql = "
			SELECT
				MAX(c.category_id) id, 
				MAX(cd.name) value,
				MAX(c.image) image,
				COUNT(*) total 
			FROM `{$this->dbPrefix('category')}` c 
			LEFT JOIN `{$this->dbPrefix('category_description')}` cd ON (c.category_id = cd.category_id) 
			LEFT JOIN `{$this->dbPrefix('category_to_store')}` c2s ON (c.category_id = c2s.category_id) 
			LEFT JOIN `{$this->dbPrefix('product_to_category')}`p2c ON (c.category_id = p2c.category_id)  
			LEFT JOIN `{$this->dbPrefix('product')}` p ON (p.product_id = p2c.product_id)
		";

		$sql .= $this->addFilters(static::$filter_data, 'category');

		$sql .= " 
			AND cd.language_id = '" . (int)$this->config_language_id . "' 
			AND c2s.store_id = '" . (int)$this->config_store_id . "' 
			AND c.status = '1'
		";

		if ($filter_category_id = Arr::get(static::$filter_data, 'filter_category_id')) {
			$sql .= " AND c.parent_id = '" . $this->db->escape($filter_category_id) . "'";
		}

		$sql .= " 
			GROUP BY c.category_id 
			HAVING COUNT(*) > 0 
			ORDER BY LCASE(cd.name) ASC
		";

		return $this->dbQuery($sql, 'CATEGORIES')->rows;
	}

	public function getManufacturers() {
		if (Arr::get(static::$filter_data, 'filter_manufacturer_id')) {
			return array();
		}

		$sql = "
			SELECT 
				max(m.manufacturer_id) id, 
				MAX(m.name) value, 
				MAX(m.image) image, 
				COUNT(*) total 
			FROM `" . DB_PREFIX . "manufacturer` m 
			LEFT JOIN `" . DB_PREFIX . "manufacturer_to_store` m2s ON (m.manufacturer_id = m2s.manufacturer_id) 
			LEFT JOIN `" . DB_PREFIX . "product` p ON (p.manufacturer_id = m.manufacturer_id)
		";

		$sql .= $this->addFilters(static::$filter_data, 'manufacturer');

		$sql .= " AND m2s.store_id = '" . (int)$this->config_store_id . "'";

		$sql .= " 
			GROUP BY m.manufacturer_id 
			HAVING COUNT(*) > 0 
			ORDER BY m.name, m.sort_order ASC
		";

		return $this->dbQuery($sql, 'MANUFACTURERS')->rows;
	}

	public function getAttributes() {
		$attribute_table = $this->journal3->settings->get('filterAttributeValuesSeparator') ? 'journal3_product_attribute' : 'product_attribute';

		$sql = "
			SELECT 
				MAX(a.attribute_id) attribute_id, 
				MAX(ad.name) attribute_name, 
				MAX(pa.text) value, 
				COUNT(*) total 
			FROM `" . DB_PREFIX . "product` p
			LEFT JOIN `" . DB_PREFIX . $attribute_table . "` pa ON (p.product_id = pa.product_id)
			LEFT JOIN `" . DB_PREFIX . "attribute` a ON (a.attribute_id = pa.attribute_id) 
			LEFT JOIN `" . DB_PREFIX . "attribute_description` ad ON (ad.attribute_id = a.attribute_id)
		";

		$sql .= $this->addFilters(static::$filter_data, 'attribute');

		$sql .= "
				AND pa.language_id = '" . (int)$this->config_language_id . "' 
				AND ad.language_id = '" . (int)$this->config_language_id . "' 
			GROUP BY lower(pa.text), a.attribute_id 
			HAVING COUNT(*) > 0
		";

		$query = $this->dbQuery($sql, 'attributes');

		$results = array();

		foreach ($query->rows as $row) {
			if (!isset($results[$row['attribute_id']])) {
				$results[$row['attribute_id']] = array(
					'attribute_id'   => $row['attribute_id'],
					'attribute_name' => $row['attribute_name'],
					'values'         => array(),
				);
			}
			$results[$row['attribute_id']]['values'][] = array(
				'id'    => $row['value'],
				'value' => $row['value'],
				'total' => $row['total'],
			);
		}

		return $results;
	}

	public function getOptions() {
		$sql = "
			SELECT 
				MAX(pov.option_id) option_id, 
				MAX(od.name) option_name, 
				MAX(ovd.option_value_id) id, 
				MAX(ovd.name) value, 
				COUNT(*) total, 
				ov.image image
			FROM `" . DB_PREFIX . "product` p
			LEFT JOIN `" . DB_PREFIX . "product_option_value` pov ON (p.product_id = pov.product_id)
			LEFT JOIN `" . DB_PREFIX . "option_value` ov ON (pov.option_value_id = ov.option_value_id) 
			LEFT JOIN `" . DB_PREFIX . "option_value_description` ovd ON (pov.option_value_id = ovd.option_value_id) 
			LEFT JOIN `" . DB_PREFIX . "option_description` od ON (pov.option_id = od.option_id)
		";

		$sql .= $this->addFilters(static::$filter_data, 'option');

		$sql .= "
				AND od.language_id = '" . (int)$this->config_language_id . "' 
				AND ovd.language_id = '" . (int)$this->config_language_id . "'
		";

		if ($this->journal3->settings->get('filterCheckOptionsQuantity')) {
			$sql .= " AND pov.quantity > 0 ";
		}

		$sql .= "
				
			GROUP BY 
				pov.option_value_id 
			HAVING 
				COUNT(*) > 0 
			ORDER BY 
				ov.sort_order, ovd.name
		";

		$query = $this->dbQuery($sql, 'OPTIONS');

		$results = array();

		foreach ($query->rows as $row) {
			if (!isset($results[$row['option_id']])) {
				$results[$row['option_id']] = array(
					'option_id'   => $row['option_id'],
					'option_name' => $row['option_name'],
					'values'      => array(),
				);
			}
			$results[$row['option_id']]['values'][] = array(
				'id'    => $row['id'],
				'value' => $row['value'],
				'image' => $row['image'],
				'total' => $row['total'],
			);
		}

		return $results;
	}

	public function getFilters() {
		$sql = "
			SELECT
				f.filter_id id,
				fd.name filter_name,
				fg.filter_group_id filter_group_id,
				fgd.name filter_group_name,
				COUNT(*) total
			FROM `" . DB_PREFIX . "product` p
			INNER JOIN `" . DB_PREFIX . "product_filter` pf ON (p.product_id = pf.product_id)
			INNER JOIN `" . DB_PREFIX . "filter` f ON (f.filter_id = pf.filter_id)
			INNER JOIN `" . DB_PREFIX . "filter_description` fd ON (fd.filter_id = pf.filter_id)
			INNER JOIN `" . DB_PREFIX . "filter_group` fg ON (fg.filter_group_id = fd.filter_group_id)
			INNER JOIN `" . DB_PREFIX . "filter_group_description` fgd ON (fd.filter_group_id = fgd.filter_group_id)
		";

		$sql .= $this->addFilters(static::$filter_data, 'filter');

		$sql .= "
				AND fd.language_id = '" . (int)$this->config_language_id . "' 
				AND fgd.language_id = '" . (int)$this->config_language_id . "'
			GROUP BY pf.filter_id 
			HAVING COUNT(*) > 0 
		";

		$query = $this->dbQuery($sql, 'FILTERS');

		$results = array();

		foreach ($query->rows as $row) {
			if (!isset($results[$row['filter_group_id']])) {
				$results[$row['filter_group_id']] = array(
					'filter_group_id'   => $row['filter_group_id'],
					'filter_group_name' => $row['filter_group_name'],
					'values'            => array(),
				);
			}
			$results[$row['filter_group_id']]['values'][] = array(
				'id'    => $row['id'],
				'value' => $row['filter_name'],
				'total' => $row['total'],
			);
		}

		return $results;
	}

	public function getTags() {
		$sql = "
			SELECT tag 
			FROM `" . DB_PREFIX . "product` p
		";

		$sql .= $this->addFilters(static::$filter_data, 'tags');

		$query = $this->dbQuery($sql, 'TAGS');

		$results = array();

		foreach ($query->rows as $row) {
			foreach (explode(",", $row['tag']) as $value) {
				$value = trim($value);

				if (strlen($value) <= 1) {
					continue;
				}

				if (!isset($results[$value])) {
					$results[$value] = array(
						'id'    => $value,
						'value' => $value,
						'total' => 1,
					);
				} else {
					$results[$value]['total']++;
				}
			}
		}

		return $results;
	}

	public function getProductIds($filter_data = null) {
		if ($filter_data === null) {
			$filter_data = static::$filter_data;
		}
		if (!in_array(Arr::get($filter_data, 'sort'), self::$SORT)) {
			$filter_data['sort'] = self::$SORT[0];
		}

		if (!in_array(Arr::get($filter_data, 'order'), self::$ORDER)) {
			$filter_data['order'] = self::$ORDER[0];
		}

		$sql = "
			SELECT 
				p.product_id, 
				(
					SELECT AVG(rating) total 
					FROM `" . DB_PREFIX . "review` r1 
					WHERE 
						r1.product_id = p.product_id 
						AND r1.status = '1' 
					GROUP BY r1.product_id
				) rating, 
				(
					SELECT price 
					FROM `" . DB_PREFIX . "product_discount` pd2 
					WHERE 
						pd2.product_id = p.product_id 
						AND pd2.customer_group_id = '" . (int)$this->config_customer_group_id . "' 
						AND pd2.quantity = '1' 
						AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) 
						AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) 
					ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1
				) discount, 
				(
					SELECT price 
					FROM `" . DB_PREFIX . "product_special` ps 
					WHERE 
						ps.product_id = p.product_id 
						AND ps.customer_group_id = '" . (int)$this->config_customer_group_id . "' 
						AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) 
						AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) 
					ORDER BY ps.priority ASC, ps.price ASC LIMIT 1
				) special,
				p.viewed 			
		";

		if (Arr::get($filter_data, 'sort') === 'sales') {
			$sql .= "
				, SUM(op.quantity) AS sales
				FROM `" . DB_PREFIX . "order_product` op
				LEFT JOIN `" . DB_PREFIX . "order` o ON (o.order_id = op.order_id)
				LEFT JOIN `" . DB_PREFIX . "product` p ON (p.product_id = op.product_id)
			";
		} else {
			$sql .= " FROM `" . DB_PREFIX . "product` p";
		}

		$sql .= $this->addFilters($filter_data);

		if (Arr::get($filter_data, 'bestseller')) {
			$sql .= " AND o.order_status_id > '0'";
		}

		$sql .= " GROUP BY p.product_id";

		if ($filter_data['sort'] === 'random') {
			$sql .= " ORDER BY RAND()";
		} else {
			if ($filter_data['sort'] === 'pd.name' || $filter_data['sort'] === 'p.model') {
				$sql .= " ORDER BY LCASE(" . $filter_data['sort'] . ")";
			} elseif ($filter_data['sort'] === 'p.price') {
				$sql .= " ORDER BY (CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)";
			} else {
				$sql .= " ORDER BY " . $filter_data['sort'];
			}

			if ($filter_data['order'] === 'DESC') {
				$sql .= " DESC, LCASE(pd.name) DESC";
			} else {
				$sql .= " ASC, LCASE(pd.name) ASC";
			}

		}

		if (Arr::get($filter_data, 'limit')) {
			$sql .= " LIMIT " . Arr::get($filter_data, 'start', 0) . ", " . $filter_data['limit'];
		}

		return $this->dbQuery($sql, 'PRODUCTS')->rows;
	}

	public function getProducts($filter_data = null) {
		$products = $this->getProductIds($filter_data);

		$product_ids = array();

		foreach ($products as $product) {
			$product_ids[$product['product_id']] = (int)$product['product_id'];
		}

		return $this->model_journal3_product->getProduct($product_ids);
	}

	public function getTotalProducts() {
		$sql = "
			SELECT COUNT(DISTINCT p.product_id) total 
			FROM `" . DB_PREFIX . "product` p
		";

		$sql .= $this->addFilters(static::$filter_data);

		return $this->dbQuery($sql, 'TOTAL_PRODUCTS')->row['total'];
	}

	private function addFilters($filter_data, $query = null) {
		$sql = "";

		if ($query !== 'category' && (Arr::get($filter_data, 'categories') || Arr::get($filter_data, 'filter_category_id'))) {
			$sql .= " LEFT JOIN `" . DB_PREFIX . "product_to_category` p2c ON (p2c.product_id = p.product_id)";

			if (Arr::get($filter_data, 'filter_sub_category')) {
				$sql .= " LEFT JOIN `" . DB_PREFIX . "category_path` cp ON (cp.category_id = p2c.category_id)";
			}
		}

		$sql .= " 
			LEFT JOIN `" . DB_PREFIX . "product_description` pd ON (p.product_id = pd.product_id)
			LEFT JOIN `" . DB_PREFIX . "product_to_store` p2s ON (p.product_id = p2s.product_id)
			WHERE 
				p.status = '1' 
				AND p.date_available <= NOW() 
				AND p2s.store_id = '" . (int)$this->config_store_id . "'
				AND pd.language_id = '" . (int)$this->config_language_id . "'
		";

		if ($query !== 'category' && (Arr::get($filter_data, 'categories') || Arr::get($filter_data, 'filter_category_id'))) {
			if (Arr::get($filter_data, 'filter_sub_category')) {
				if (Arr::get($filter_data, 'categories')) {
					$sql .= " AND cp.path_id IN (" . implode(",", $filter_data['categories']) . ")";
				} else {
					$sql .= " AND cp.path_id = '" . (int)$filter_data['filter_category_id'] . "'";
				}
			} else {
				if (Arr::get($filter_data, 'categories')) {
					$sql .= " AND p2c.category_id IN (" . implode(",", $filter_data['categories']) . ")";
				} else {
					$sql .= " AND p2c.category_id = '" . (int)$filter_data['filter_category_id'] . "'";
				}
			}
		}

		if ($query !== 'manufacturer') {
			if (Arr::get($filter_data, 'manufacturers')) {
				$sql .= " AND p.manufacturer_id IN (" . implode(",", $filter_data['manufacturers']) . ")";
			} else if (Arr::get($filter_data, 'filter_manufacturer_id')) {
				$sql .= " AND p.manufacturer_id = '" . (int)$filter_data['filter_manufacturer_id'] . "'";
			}
		}

		if ($query !== 'attribute' && Arr::get($filter_data, 'attributes')) {
			$attribute_table = $this->journal3->settings->get('filterAttributeValuesSeparator') ? 'journal3_product_attribute' : 'product_attribute';

			foreach ($filter_data['attributes'] as $attribute_id => $attribute_values) {
				$temp = array();

				foreach ($attribute_values as $key => $value) {
					$temp[] = "TRIM(pai.text) = '" . $this->db->escape($value) . "'";
				}

				$sql .= " AND EXISTS (SELECT * FROM `" . DB_PREFIX . $attribute_table . "` pai WHERE p.product_id = pai.product_id AND pai.attribute_id = " . $attribute_id . " AND (" . implode(' OR ', $temp) . "))";
			}
		}

		if ($query !== 'option' && Arr::get($filter_data, 'options')) {
			foreach ($filter_data['options'] as $options) {
				$temp = array();

				foreach ($options as $option) {
					$temp[] = $this->db->escape($option);
				}

				$sql .= " AND EXISTS (
							SELECT * FROM `" . DB_PREFIX . "product_option_value` povi 
							WHERE
								p.product_id = povi.product_id AND povi.option_value_id IN (" . implode(', ', $temp) . ")
				";

				if ($this->journal3->settings->get('filterCheckOptionsQuantity')) {
					$sql .= " AND povi.quantity > 0 ";
				}

				$sql .= ")";
			}
		}

		if ($query !== 'filter' && Arr::get($filter_data, 'filters')) {
			foreach ($filter_data['filters'] as $filters) {
				$temp = array();

				foreach ($filters as $filter) {
					$temp[] = $this->db->escape($filter);
				}

				$sql .= " AND EXISTS (SELECT * FROM `" . DB_PREFIX . "product_filter` pfi WHERE p.product_id = pfi.product_id AND pfi.filter_id IN (" . implode(', ', $temp) . "))";
			}
		}

		if (Arr::get($filter_data, 'special')) {
			$sql .= " AND EXISTS (SELECT product_id FROM `" . DB_PREFIX . "product_special` ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config_customer_group_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())))";
		}

		if (Arr::get($filter_data, 'filter_name') || Arr::get($filter_data, 'tags')) {
			$sql .= " AND (";

			if (isset($filter_data['filter_name']) && strlen($filter_data['filter_name']) > 0) {
				$implode = array();

				$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $filter_data['filter_name'])));

				foreach ($words as $word) {
					$implode[] = " pd.name LIKE '%" . $this->db->escape($word) . "%'";
				}

				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}

				if (isset($filter_data['description']) && $filter_data['description'] == 1) {
					$sql .= " OR pd.description LIKE '%" . $this->db->escape($filter_data['filter_name']) . "%'";
				}
			}

			if ((isset($filter_data['filter_name']) && strlen($filter_data['filter_name']) > 0) && (isset($filter_data['tags']) && !empty($filter_data['tags']))) {
				$sql .= " OR ";
			}

			if (isset($filter_data['tags']) && !empty($filter_data['tags'])) {

				$sql .= "";
				foreach ($filter_data['tags'] as $key => $tag) {
					if ($key == 0) {
						$sql .= " pd.tag LIKE '%" . $this->db->escape($tag) . "%'";
					} else {
						$sql .= " OR pd.tag LIKE '%" . $this->db->escape($tag) . "%'";
					}
				}
				$sql .= "";
			}

			if (isset($filter_data['filter_name']) && strlen($filter_data['filter_name']) > 0) {
				$sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($filter_data['filter_name'])) . "'";
			}

			if (isset($filter_data['filter_name']) && strlen($filter_data['filter_name']) > 0) {
				$sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($filter_data['filter_name'])) . "'";
			}

			if (isset($filter_data['filter_name']) && strlen($filter_data['filter_name']) > 0) {
				$sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($filter_data['filter_name'])) . "'";
			}

			if (isset($filter_data['filter_name']) && strlen($filter_data['filter_name']) > 0) {
				$sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($filter_data['filter_name'])) . "'";
			}

			if (isset($filter_data['filter_name']) && strlen($filter_data['filter_name']) > 0) {
				$sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($filter_data['filter_name'])) . "'";
			}

			if (isset($filter_data['filter_name']) && strlen($filter_data['filter_name']) > 0) {
				$sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($filter_data['filter_name'])) . "'";
			}

			if (isset($filter_data['filter_name']) && strlen($filter_data['filter_name']) > 0) {
				$sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($filter_data['filter_name'])) . "'";
			}

			$sql .= ")";
		}

		if ($query !== 'price' && Arr::get($filter_data, 'price')) {
			$discount = "
				(
					SELECT price 
					FROM `" . DB_PREFIX . "product_discount` pd2 
					WHERE 
						pd2.product_id = p.product_id 
						AND pd2.customer_group_id = '" . (int)$this->config_customer_group_id . "' 
						AND pd2.quantity = '1' 
						AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) 
						AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) 
					ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1
				)
			";

			$special = "
				(
					SELECT price 
					FROM `" . DB_PREFIX . "product_special` ps 
					WHERE 
						ps.product_id = p.product_id 
						AND ps.customer_group_id = '" . (int)$this->config_customer_group_id . "' 
						AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) 
						AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) 
					ORDER BY ps.priority ASC, ps.price ASC LIMIT 1
				)
			";

			$sql .= " AND COALESCE(" . $special . ", " . $discount . ", p.price) BETWEEN " . (float)$this->undoTax($filter_data['price']['min']) . " AND " . (float)$this->undoTax($filter_data['price']['max']) . "";
		}

		if ($availability = Arr::get($filter_data, 'availability')) {
			$quantity = null;

			if (is_array($availability) && count($availability) === 1) {
				if ($availability[0] === '0') {
					$quantity = false;
				} else if ($availability[0] === '1') {
					$quantity = true;
				}
			} else {
				if ($availability === '0') {
					$quantity = false;
				} else if ($availability === '1') {
					$quantity = true;
				}
			}

			if ($quantity === true) {
				$sql .= ' AND p.quantity > 0';
			} else if ($quantity === false) {
				$sql .= ' AND p.quantity <= 0';
			}
		}

		return $sql;
	}

	private function applyTax($price) {
		if ($this->tax_class_id) {
			$price = $this->tax->calculate($price, $this->journal3->settings->get('filterTaxClassId'), $this->config_tax);
		}

		return $price * $this->currency->getValue($this->session->data['currency']);
	}

	private function undoTax($price) {
		if (!$price) {
			return $price;
		}

		$tax_rates = $this->tax->getRates($price, $this->journal3->settings->get('filterTaxClassId'));

		$percent = 0;

		foreach ($tax_rates as $tax_rate) {
			if ($tax_rate['type'] == 'F') {
				$price -= $tax_rate['rate'];
			} elseif ($tax_rate['type'] == 'P') {
				$percent += $tax_rate['rate'];
			}
		}

		if ($percent != 0) {
			$price /= (1 + ($percent / 100));
		}

		return $price / $this->currency->getValue($this->session->data['currency']);
	}

	protected function dbQuery($sql, $tag = null) {
		\Journal3\Utils\Log::debug($sql, $tag);

		$result = parent::dbQuery($sql, $tag);

		\Journal3\Utils\Log::debug(json_encode($result, JSON_PRETTY_PRINT), $tag . '_RESULT');

		return $result;
	}

}
